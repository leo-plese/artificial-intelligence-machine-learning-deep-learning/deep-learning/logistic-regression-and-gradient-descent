Logistic regression and Gradient Descent. Tasks:

1 Creating artificial 2D dataset

2 Learning binary logistic regression by gradient descent

3 Graphical representation of classification results

4 Plotting a decision function

5 Graphical representation of binary logistic regression

6 Multiclass logistic regression

Implemented in Python using NumPy and Matplotlib library.

My lab assignment in Deep Learning, FER, Zagreb.

Created: 2021
