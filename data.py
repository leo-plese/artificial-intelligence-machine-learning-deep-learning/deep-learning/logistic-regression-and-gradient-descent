import numpy as np
import matplotlib.pyplot as plt

# zad 1
class Random2DGaussian():
    def __init__(self):
        self.minx = 0
        self.maxx = 10
        self.miny = 0
        self.maxy = 10

        mux = np.random.random_sample()*(self.maxx-self.minx)
        muy = np.random.random_sample() * (self.maxy - self.miny)
        self.mu = np.array([mux, muy])

        eigvalx = (np.random.random_sample()*(self.maxx-self.minx)/5)**2
        eigvaly = (np.random.random_sample() * (self.maxy - self.miny) / 5) ** 2

        D = np.diag(np.array([eigvalx, eigvaly]))

        phi = np.random.random_sample() * 2*np.pi
        R = np.array([[np.cos(phi), -np.sin(phi)], [np.sin(phi), np.cos(phi)]])

        rt = R.T
        self.sigma = rt.dot(D).dot(R)
        return



    def get_sample(self, n):
        return np.random.multivariate_normal(self.mu, self.sigma, n)

# zad 2

def sigmoid(scores):
    return 1 / (1 + np.exp(-scores))

def binlogreg_train(X, Y_, param_niter=10000, param_delta=0.1):
    '''
        Argumenti
          X:  podatci, np.array NxD
          Y_: indeksi razreda, np.array Nx1

        Povratne vrijednosti
          w, b: parametri logističke regresije
    '''
    N, D = X.shape
    w = np.random.randn(D)
    w  = np.reshape(w, (-1,1))
    b = 0

    Yoh = np.hstack([np.where(Y_ == 0, 1, 0), np.where(Y_ == 1, 1, 0)])

    # gradijentni spust (param_niter iteracija)
    for i in range(param_niter):
        # klasifikacijske mjere
        scores = X.dot(w) + b  # N x 1

        # vjerojatnosti razreda c_1
        probs1 = sigmoid(scores)  # N x 1
        probs0 = 1-probs1
        probs = np.hstack([probs0, probs1])


        # gubitak
        probs_correct_class = np.multiply(probs, Yoh).flatten()
        probs_correct_class = probs_correct_class[probs_correct_class != 0]
        loss = -np.mean(np.log(probs_correct_class))  # scalar


        # # dijagnostički ispis
        if i % 10 == 0:
            print("iteration {}: loss {}".format(i, loss))
            # print("w",w)
            # print("b", b)
            # print()

        # derivacije gubitka po klasifikacijskim mjerama
        dL_dscores = probs1 - Y_ # N x 1

        # gradijenti parametara
        grad_w = 1/N * np.dot(dL_dscores.T, X).T  # D x 1
        grad_b = np.mean(dL_dscores)  # 1 x 1

        # poboljšani parametri
        w += -param_delta * grad_w
        b += -param_delta * grad_b

    return w, b, loss

def binlogreg_classify(X, w, b):
    '''
      Argumenti
          X:    podatci, np.array NxD
          w, b: parametri logističke regresije

      Povratne vrijednosti
          probs: vjerojatnosti razreda c1
    '''
    scores = X.dot(w) + b
    probs = sigmoid(scores)

    return probs

def sample_gauss_2d(C, N):
    x, y = [], []
    rngs = []
    for i in range(C):
        rnd_2d_gauss_dist = Random2DGaussian()
        rngs.append(rnd_2d_gauss_dist)
        y.append([i for _ in range(N)])

    x = [g.get_sample(N) for g in rngs]
    X = np.reshape(x, (N*C, -1))
    Y_ = np.reshape(y, (N*C, -1))

    return X, Y_


def get_tp_tn_fp_fn_binary(Y, Y_):
    eq = (Y == Y_)
    neq = (Y != Y_)

    y0 = (Y == 0)
    y1 = (Y == 1)

    tp = np.sum(np.logical_and(eq, y1))
    tn = np.sum(np.logical_and(eq, y0))
    fp = np.sum(np.logical_and(neq, y1))
    fn = np.sum(np.logical_and(neq, y0))

    return tp, tn, fp, fn

def get_accuracy(tp, tn, N):
    return (tp + tn) / N

def get_recall(tp, fn):
    return tp / (tp + fn)

def get_precision(tp, fp):
    if (tp + fp) == 0:
        return None
    return tp / (tp + fp)

def eval_perf_binary(Y,Y_):
    tp, tn, fp, fn = get_tp_tn_fp_fn_binary(Y, Y_)

    accuracy = get_accuracy(tp, tn, len(Y))
    recall = get_recall(tp, fn)
    precision = get_precision(tp, fp)

    return accuracy, recall, precision

def eval_AP(Y_):
    N = len(Y_)
    Y_ = np.array(Y_)
    gt_1_inds = np.where(Y_ == 1)[0]
    if len(gt_1_inds) == 0:
        return None

    precision_sum = 0
    for i in gt_1_inds:
        y0 = [0 for _ in range(i)]
        y1 = [1 for _ in range(i, N)]
        Y = np.array(y0 + y1)
        tp, _, fp, _ = get_tp_tn_fp_fn_binary(Y, Y_)
        precision_sum += get_precision(tp, fp)
    ap = precision_sum / len(gt_1_inds)

    return ap

##################
# actual x predicted
# classes: from 0 to C-1 (top to bottom (predicted), left to right (actual))
def get_confusion_matrix(Y, Y_, C):
    conf_mat = np.zeros((C,C))
    for i in range(C):
        Y_pred_i = Y[np.where(Y_ == i)]

        for x in Y_pred_i:
            conf_mat[i][x] += 1

    conf_mat = conf_mat.T

    return conf_mat

# c_ind - class index
# classes: 0, 1 (top to bottom (predicted), left to right (actual))
def get_conf_mat_for_class(multi_conf_mat, c_ind, C):

    tp = multi_conf_mat[c_ind][c_ind]
    tn = sum([multi_conf_mat[i][j] if i != c_ind and j != c_ind else 0 for i in range(C) for j in range(C)])
    fp = sum([multi_conf_mat[c_ind][i] if i != c_ind else 0 for i in range(C)])
    fn = sum([multi_conf_mat[i][c_ind] if i != c_ind else 0 for i in range(C)])

    conf_mat = np.array([[tn,fn],[fp, tp]])

    return conf_mat

def eval_perf_multi(Y, Y_):
    # Y = np.array([0,2,1,1,2,1,1,0,2,2,1,2])
    # Y_ = np.array([0,0,0,0,1,1,1,1,2,2,2,2])

    C = max(Y_)[0] + 1  # 3
    N = len(Y_)
    # # example - confusion matrix
    # conf_mat = get_confusion_matrix(Y, Y_, C)
    conf_mat = get_confusion_matrix(Y.flatten(), Y_.flatten(), C)

    conf_mats_by_cls = np.zeros((C, 2, 2))
    accuracy_by_cls, precision_by_cls, recall_by_cls = np.zeros(C), np.zeros(C), np.zeros(C)
    for i in range(C):
        conf_mat_for_class_i = get_conf_mat_for_class(conf_mat, i, C)
        conf_mats_by_cls[i] = conf_mat_for_class_i

        accuracy_by_cls[i] = get_accuracy(conf_mat_for_class_i[1][1], conf_mat_for_class_i[0][0], N)
        precision_by_cls[i] = get_precision(conf_mat_for_class_i[1][1], conf_mat_for_class_i[1][0])
        recall_by_cls[i] = get_recall(conf_mat_for_class_i[1][1], conf_mat_for_class_i[0][1])

    accuracy_avg = np.mean(accuracy_by_cls)
    precision_avg = np.mean(precision_by_cls)
    recall_avg = np.mean(recall_by_cls)

    # accuracy score (where Y == Y_)
    accuracy_score = np.mean(Y == Y_)


    return conf_mats_by_cls, accuracy_score, accuracy_avg, precision_avg, recall_avg, accuracy_by_cls, precision_by_cls, recall_by_cls


def get_one_hot_matrix(cls_vect_Y_, N, C):
    Y_oh = np.zeros((N, C))
    Y_oh[np.arange(N), cls_vect_Y_] = 1
    return Y_oh

def stable_softmax_multi(scores, N, C):
    expscores = np.exp(scores - np.amax(scores, axis=1).reshape((N, -1)))  # N x C

    # nazivnik sofmaksa
    sumexp = np.sum(expscores, axis=1)  # N x 1

    # logaritmirane vjerojatnosti razreda
    probs = expscores / np.tile(sumexp.reshape((N, -1)), C)  # N x C

    return probs

def logreg_train(X, Y_, param_niter=10000, param_delta=0.1):
    '''
        Argumenti
          X:  podatci, np.array NxD
          Y_: indeksi razreda, np.array NxC

        Povratne vrijednosti
          W, b: parametri logističke regresije
    '''
    C = (max(Y_) + 1)[0]
    N, D = X.shape
    Xt = X.T
    W = np.random.randn(C, D)   # C x D
    b = np.zeros((C, 1))     # C x 1
    Yoh = get_one_hot_matrix(Y_.flatten(), N, C)

    # gradijentni spust (param_niter iteracija)
    for i in range(param_niter):
        # klasifikacijske mjere
        scores = (W.dot(Xt) + b).T  # N x C
        probs = stable_softmax_multi(scores, N, C)

        # logaritmirane vjerojatnosti razreda
        logprobs = np.log(probs)  # N x C

        # gubitak
        logprobs_correct_class = (logprobs*Yoh).flatten()
        logprobs_correct_class = logprobs_correct_class[logprobs_correct_class != 0]
        loss = -np.mean(logprobs_correct_class)  # scalar


        # dijagnostički ispis
        if i % 10 == 0:
            print("iteration {}: loss {}".format(i, loss))
            # print("W",W)
            # print("b", b)
            # print()

        # derivacije gubitka po klasifikacijskim mjerama
        dL_dscores = probs - Yoh # N x C

        # gradijenti parametara
        grad_W = 1/N * np.dot(dL_dscores.T, X)  # C x D
        grad_b = np.mean(dL_dscores.T, axis=1).reshape((C,1))  # C x 1

        # poboljšani parametri
        W += -param_delta * grad_W
        b += -param_delta * grad_b

    return W, b, loss

def logreg_classify(C, X, W, b):
    '''
      Argumenti
          X:    podatci, np.array NxD
          W, b: parametri logističke regresije

      Povratne vrijednosti
          probs: vjerojatnosti razreda
    '''
    N = len(X)
    scores = (W.dot(X.T) + b).T
    probs = stable_softmax_multi(scores, N, C)

    return probs

###################
def myDummyDecision(X):
    scores = X[:,0] + X[:,1] - 5
    return scores

def graph_data(X, Y_, Y, C=2, graph_multi=False):
    '''
      X  ... podatci (np.array dimenzija Nx2)
      Y_ ... točni indeksi razreda podataka (Nx1)
      Y  ... predviđeni indeksi razreda podataka (Nx1)
      C  ... broj klasa
    '''
    Y = np.reshape(Y, (len(Y_),-1))

    BOTTOM_COLOR_LIM = 0.3
    TOP_COLOR_LIM = 1

    if graph_multi:
        palette = np.arange(C) / (C - 1) * (TOP_COLOR_LIM-BOTTOM_COLOR_LIM) + BOTTOM_COLOR_LIM
    else:
        palette = [[BOTTOM_COLOR_LIM for _ in range(3)], [TOP_COLOR_LIM for _ in range(3)]]
    colors = np.zeros(shape=(len(Y),3))

    for i in range(len(palette)):
        colors[(Y_==i).flatten()] = palette[i]

    eq_inds = np.where(Y == Y_)[0]
    neq_inds = np.where(Y != Y_)[0]
    plt.scatter(X[eq_inds,0], X[eq_inds,1], s=20, c=colors[eq_inds], edgecolors='k',marker='o')
    plt.scatter(X[neq_inds,0], X[neq_inds,1], s=20, c=colors[neq_inds], edgecolors='k',marker='s')
    # for i in range(len(X)):
    #     print(X[i], Y_[i], Y[i])

def graph_surface(fun,rect,offset=0.5,width=256,height=256,graph_multi=False):
    '''
      fun    ... decizijska funkcija (Nx2)->(Nx1)
      rect   ... željena domena prikaza zadana kao:
                 ([x_min,y_min], [x_max,y_max])
      offset ... "nulta" vrijednost decizijske funkcije na koju
                 je potrebno poravnati središte palete boja;
                 tipično imamo:
                 offset = 0.5 za probabilističke modele
                    (npr. logistička regresija)
                 offset = 0 za modele koji ne spljošćuju
                    klasifikacijske mjere (npr. SVM)
      width,height ... rezolucija koordinatne mreže
    '''
    x_min, y_min = rect[0]
    x_max, y_max = rect[1]
    xx, yy = np.linspace(x_min, x_max, width), np.linspace(y_min, y_max, height)
    X, Y = np.meshgrid(xx, yy)

    coord = np.stack([X.flatten(), Y.flatten()], axis=1)    # axis=1 -> vertical matrix Nx2

    dec_fun_vals = fun(coord)
    if graph_multi:
        # 1. nacin: vjerojatnost neke klase
        # dec_fun_vals = dec_fun_vals[:,0].reshape((-1,1))    # OK
        ## dec_fun_vals = dec_fun_vals[:, 1].reshape((-1, 1))  # OK

        # 2. nacin: max vjerojatnost klasifikacije u bilo koju klasu
        # dec_fun_vals = np.amax(dec_fun_vals, axis=1).reshape((-1,1))      # OK

        # 3. nacin: klasa koja ima najvecu aposteriornu vjerojatnost
        dec_fun_vals = np.argmax(dec_fun_vals, axis=1).reshape((-1, 1))   # OK

    # dec_fun_vals = np.reshape(dec_fun_vals, (width, height))  # njihov
    dec_fun_vals = np.reshape(dec_fun_vals, (height, width))

    # # fix the range and offset
    maxval = max(np.max(dec_fun_vals) - offset, - (np.min(dec_fun_vals) - offset))

    # draw the surface and the offset
    plt.pcolormesh(X, Y, dec_fun_vals, vmin=offset - maxval, vmax=offset + maxval, cmap=plt.get_cmap('jet'), shading='auto')
    # plt.pcolormesh(X, Y, dec_fun_vals,cmap=plt.get_cmap('jet'))

    plt.contour(X, Y, dec_fun_vals, colors='k', levels=[offset])

def binlogreg_decfun(w, b):
    def classify(X):
        return binlogreg_classify(X, w, b)
    return classify

def logreg_decfun(C, w, b):
    def classify(X):
        return logreg_classify(C, X, w, b)
    return classify


if __name__ == "__main__":
    np.random.seed(100)

    C = 9#2#4
    N = 100

    # zad 6
    # instantiate the dataset
    X, Y_ = sample_gauss_2d(C, N)

    # train the logistic regression model
    W,b,loss = logreg_train(X, Y_)
    print("W =", W)
    print("mean(W) =", W.mean())
    print("std(W) =", W.std())
    print("b =", b)
    print("CE loss =", loss)

    # evaluate the model on the train set
    probs = logreg_classify(C, X, W, b)

    # recover the predicted classes Y
    Y = np.argmax(probs, axis=1).reshape((len(X), -1))

    # # evaluate and print performance measures
    conf_mats_by_cls, accuracy_score, accuracy_avg, precision_avg, recall_avg, accuracy_by_cls, precision_by_cls, recall_by_cls = eval_perf_multi(Y, Y_)
    # Yoh = get_one_hot_matrix(Y_.flatten(), N * C, C)
    # sorted_Y_by_pred_probs = Y_[(probs*Yoh).argsort(axis=0)[:,0]].flatten()
    print()
    print("confusion matrix =")
    for i in range(C):
        conf_mat_cls_i = conf_mats_by_cls[i]
        print("y =", i)
        print(conf_mat_cls_i)
    print("accuracy score =", accuracy_score)
    print("avg(accuracy) =", accuracy_avg)
    print("avg(precision) =", precision_avg)
    print("avg(recall) =", recall_avg)
    print("accuracy by classes =", accuracy_by_cls)
    print("precision by classes =", precision_by_cls)
    print("recall by classes =", recall_by_cls)

    # # graph the decision surface
    decfun = logreg_decfun(C, W, b)
    bbox = (np.min(X, axis=0), np.max(X, axis=0))
    graph_surface(decfun, bbox, offset=0.5, graph_multi=True)

    # graph the data points
    graph_data(X, Y_, Y, C, graph_multi=True)

    # show the plot
    plt.show()

# if __name__ == "__main__":
#     np.random.seed(100)
#
#     # zad 5
#     # instantiate the dataset
#     X, Y_ = sample_gauss_2d(2, 100)
#
#     # train the logistic regression model
#     w,b,loss = binlogreg_train(X, Y_)
#     print("w =", w)
#     print("mean(w) =", w.mean())
#     print("std(w) =", w.std())
#     print("b =", b)
#     print("CE loss =", loss)
#
#     # evaluate the model on the train set
#     probs = binlogreg_classify(X, w,b)
#
#     # recover the predicted classes Y
#     Y = (probs >= 0.5)
#
#     # evaluate and print performance measures
#     accuracy, recall, precision = eval_perf_binary(Y, Y_)
#     sorted_Y_by_pred_probs = Y_[probs.argsort(axis=0)[:,0]].flatten()
#     # print(eval_AP([0,0,0,1,1,1]))
#     # print(eval_AP([0,0,1,0,1,1]))
#     # print(eval_AP([0,1,0,1,0,1]))
#     # print(eval_AP([1,0,1,0,1,0]))
#     AP = eval_AP(sorted_Y_by_pred_probs)
#     print (accuracy, recall, precision, AP)
#
#     # graph the decision surface
#     decfun = binlogreg_decfun(w, b)
#     bbox = (np.min(X, axis=0), np.max(X, axis=0))
#     graph_surface(decfun, bbox, offset=0.5)
#
#     # graph the data points
#     graph_data(X, Y_, Y)
#
#     # show the plot
#     plt.show()

# if __name__ == "__main__":
#     # zad 3, 4
#     np.random.seed(100)
#
#     # get the training dataset
#     X, Y_ = sample_gauss_2d(2, 100)
#     # print(X)
#     # print("******")
#     # print(Y_)
#
#
#     # # zad 4
#     bbox = (np.min(X, axis=0), np.max(X, axis=0))
#     print(bbox)
#     graph_surface(myDummyDecision, bbox, offset=0.5)
#
#     # # zad 3
#     # get the class predictions
#     Y = myDummyDecision(X) > 0.5
#
#     # graph the data points
#     graph_data(X, Y_, Y)
#
#     # show the results
#     plt.show()


# if __name__=="__main__":
#     # zad 2
#     np.random.seed(100)
#
#     # get the training dataset
#     X,Y_ = sample_gauss_2d(2, 100)
#     # X, Y_ = sample_gauss_2d(2, 2)
#
#     # train the model
#     w,b = binlogreg_train(X, Y_)
#
#     # evaluate the model on the training dataset
#     probs = binlogreg_classify(X, w,b)
#     Y = (probs >= 0.5)
#
#     # report performance
#     accuracy, recall, precision = eval_perf_binary(Y, Y_)
#     AP = eval_AP(Y_[probs.argsort()])
#     print (accuracy, recall, precision, AP)

# if __name__=="__main__":
    # # zad 1
    # G=Random2DGaussian()
    # X=G.get_sample(100)
    # plt.scatter(X[:,0], X[:,1])
    # plt.show()





